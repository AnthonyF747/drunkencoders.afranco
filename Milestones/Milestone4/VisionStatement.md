# PapaVictorSierra

## Mission Statement 

**For** many veterans across the country, it can be struggle to find adequate benefits support, or support in general. With the help of specific API's developed for the Department of Veterans Affair, **our** application looks to ease this burden. Veterans will be able to search for VA facilities in their area, and find directions to these facilities. Veterans will also be able to check reviews, or check in with other veterans on our discussion board. There will also be a live chat for those **who** would like to speak with someone, without having to call a crisis hot line. **Unlike** the current system that **is** bogged down in bureaucracy so badly, **that** many vets just give up in getting the benefit support, or even worse; get scammed out of their benefits by sites masquerading as helpful tools for veterans. 
