namespace NewsCheck.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserRating")]
    public partial class UserRating
    {
        public int UserRatingID { get; set; }

        public int UsersID { get; set; }

        [Required]
        public string UpJustification { get; set; }

        [Required]
        public string DownJustification { get; set; }

        [Required]
        public string FlagJustification { get; set; }

        public virtual User User { get; set; }
    }
}
