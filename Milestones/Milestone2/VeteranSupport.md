# ProjectVeteranSupport

We are looking to create a website with the aim of helping veterans and their family find support. That support will be in the form of locating VA Facilities, a place to review and discuss VA Facilities, and eventually a live chat system for veterans that just need someone to talk with, but don't want to call the crisis line. There are early plans to expand the features into a way for veterans to manage their benefits and ways to help manage health related things such as being able to view their medical records, schedule appointments and find specialty facilities. 

**What is new/original about this idea? What are related websites/apps? (Be able to answer the question: isn't somebody already doing this?)**

There is as surprising lack of veteran support around this country. This website application looks to help change some of that. The simple reason this web app is different? There's simply not enough support out there currently. It was a struggle to find even one related website or app. The government and Department of Veteran Affairs are pushing for developers to build more applications to help and aid veterans. 

**Why is this idea worth doing? Why is it useful and not boring?**

This is worth doing for the simple fact that veterans in this country often struggle to find help for some of the unique difficulties they often face when no longer in the service. It's useful because veterans often have problems finding VA centers and facilities in their area. 

**What are a few major features?**

-	A way to search for and get information regarding VA Facilities such as; contact information, location, hours of operation, available services, directions.
-	A message/forum type place for veterans and family members to gather and ask questions or request further information.
-	A live chat system to help those struggling to find information or those with emotional distress and looking for someone to speak with.
-   Ability to schedule an Uber or Lyft to a facility.
-   Check if public transportation has a stop near by.

**What resources will be required for you to complete this project that are not already included in the class. i.e. you already have the Microsoft stack, server, database so what else would you need? Additional API's, frameworks or platforms you'll need to use.**

-	The VA Facilities [API](https://developer.va.gov/)
    -   With plans to expand the site with other API's on the site that are available
-	Perhaps the [Google Maps API](https://developers.google.com/maps/documentation/)
-	Azure Services (SQL Database and App Services)
-   Plans for [Uber API](https://developer.uber.com/) or [Lyft API](https://www.lyft.com/developers)

**What algorithmic content is there in this project? i.e. what algorithm(s) will you have to develop or implement in order to do something central to your project idea? (Remember, this isn't just a software engineering course, it is your CS degree capstone course!)**

This one is a little tough. Currently researching more ideas, but an early one is to find a way to estimate the cost of travel to a selected VA facility for those without a vehicle.

**Rate the topic with a difficulty rating of 1-10. One being supremely easy to implement (not necessarily short though). Ten would require the best CS students using lots of what they learned in their CS degree, plus additional independent learning, to complete successfully.**

This has a current difficulty rating of 6, but could be as high as a 7. There are very few applications whether web based or mobile based to draw inspiration from. Fortunately for us the API's are there.