﻿Feature: ExampleFeature
	In order to visit the WTF site
	As a user of the internet
	I want to be able to navigate to the homepage with the URL bar.

@mytag
Scenario: Visit the homepage
	Given I have entered "http://localhost:57372/" into the URL
	When I press the "registerLink" button
	Then the result should be "http://localhost:57372/Account/Register" in the URL

