﻿using System;
using System.Collections.Generic;
using WhiskeyTangoFoxtrot.Controllers;
using WhiskeyTangoFoxtrot.Repositories.Interfaces;
using WhiskeyTangoFoxtrot.Repositories;
using WhiskeyTangoFoxtrot.DAL;
using WhiskeyTangoFoxtrot.Models;
using System.Web;
using System.Web.Mvc;
using Moq;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WTFTests
{
    [TestClass]
    public class TJUnitTests
    {
        [TestMethod]
        public void IsInputIncremented_ReturnsTrue()
        {
            //Define 
            int a = 1;

            //Create
            ProfileController prof = new ProfileController();

            //Assert
            var result = prof.IncrementInput(a);
            Assert.AreEqual(a + 1, result);
        }

        [TestMethod]
        public void IsInputIncremented_ReturnsFalse()
        {
            //Define 
            int a = 1;

            //Create
            ProfileController prof = new ProfileController();

            //Assert
            var result = prof.IncrementInput(a);
            Assert.AreNotEqual(a, result);
        }
    }

    [TestClass]
    public class ForumTests
    {
        [TestMethod]
        public void can_get_index()
        {
            Mock<IForumPostRepository> mock = new Mock<IForumPostRepository>();
            mock.Setup(a => a.ForumPosts).Returns(new ForumPost[]{
                new ForumPost {ForumPostID = 1, ForumID = 1, WTFUserID = 1, PostMessage = "Post 1 By user 1" },
                new ForumPost {ForumPostID = 2, ForumID = 1, WTFUserID = 2, PostMessage = "Post 2 By user 2" },
                new ForumPost {ForumPostID = 3, ForumID = 1, WTFUserID = 3, PostMessage = "Post 3 By user 3" },
                new ForumPost {ForumPostID = 4, ForumID = 1, WTFUserID = 1, PostMessage = "Post 4 By user 1" },
                new ForumPost {ForumPostID = 5, ForumID = 1, WTFUserID = 2, PostMessage = "Post 5 By user 2" },
                new ForumPost {ForumPostID = 6, ForumID = 2, WTFUserID = 1, PostMessage = "Post 6 By user 1, Different Forum" }
            });
            ForumController forumcontroller = new ForumController(mock.Object);

            int arrayIndex = mock.Object.ForumPosts.Count();

            Assert.IsTrue(arrayIndex != 0, "Empty Array");
            Assert.IsTrue(arrayIndex == 6);
        }

    }
}



