﻿INSERT INTO Cities VALUES
	-- Washington
	('Tacoma, WA', 47.2529, -122.4443),
	('Seattle, WA', 47.6062, -122.3321),
	('Everett, WA', 47.9790, -122.2021),
	('Mount Vernon, WA', 48.4201, -122.3375),
	('Bellingham, WA', 48.7519, -122.4787),
	-- Oregon
	('Ashland, OR', 42.1946, -122.7095),
	('Medford, OR', 42.3265, -122.8756),
	('Grants Pass, OR', 42.4390, -123.3284),
	('Roseburg, OR', 43.2165, -123.3417),
	('Eugene, OR', 44.0521, -123.0868),
	('Albany, OR', 44.6365, -123.1059),
	('Salem, OR', 44.9429, -123.0351),
	('Portland, OR', 45.5155, -122.6793),
	-- California
	('San Diego, CA', 32.7157, -117.1611),
	('Santa Ana, CA', 33.7455, -117.8677),
	('Los Angeles, CA', 34.0522, -118.2437),
	('Stockton, CA', 37.9577, -121.2908),
	('Sacramento, CA', 38.5816, -121.4944),
	('Redding, CA', 40.5865, -122.3917),
	('Mount Shasta City, CA', 41.3099, -122.3106),
	('Weed, CA', 41.4226, -122.3861),
	('Yreka, CA', 41.7354, -122.6345);
