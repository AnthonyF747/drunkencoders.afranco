namespace WhiskeyTangoFoxtrot.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class WTFUser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public WTFUser()
        {
            ChatMessages = new HashSet<ChatMessage>();
            Fora = new HashSet<Forum>();
            ForumPosts = new HashSet<ForumPost>();
        }

        public int WTFUserID { get; set; }

        [StringLength(64)]
        public string Username { get; set; }

        public int? LoginCount { get; set; }

        [Required]
        [StringLength(128)]
        public string AspNetIdentityID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChatMessage> ChatMessages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Forum> Fora { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ForumPost> ForumPosts { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
