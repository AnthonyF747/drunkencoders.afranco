namespace WhiskeyTangoFoxtrot.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class City
    {
        public int CityID { get; set; }

        [Column("City")]
        [Required]
        [StringLength(32)]
        public string City1 { get; set; }

        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }
    }
}
