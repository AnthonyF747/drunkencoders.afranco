﻿using WhiskeyTangoFoxtrot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WhiskeyTangoFoxtrot.Models.Viewmodels
{
    //View model for html page of the users profile. 
    public class ProfileViewModel
    {
    public IEnumerable<WTFUser> WTFUsers { get; set; }
    public IEnumerable<UserProfile> UserProfiles { get; set; }
    public IEnumerable<Forum> Forums { get; set; }
    public IEnumerable<ForumPost> ForumPost { get; set; }
    }
}