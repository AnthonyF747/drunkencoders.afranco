namespace WhiskeyTangoFoxtrot.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ChatBox")]
    public partial class ChatBox
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ChatBox()
        {
            ChatMessages = new HashSet<ChatMessage>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ChatBoxID { get; set; }

        [Required]
        [StringLength(64)]
        public string ChatName { get; set; }

        [Required]
        [StringLength(64)]
        public string ChatTopic { get; set; }

        [StringLength(256)]
        public string Message { get; set; }

        public bool? IsMemberOnly { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChatMessage> ChatMessages { get; set; }
    }
}
