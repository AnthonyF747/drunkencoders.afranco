﻿$(document).ready(function () {
});

//Code refactoring provided by TJ Dennis 4/13/2019 to implement API functionality
function findCity(lat, lng) {
    // Call the map building function
    buildMap(lat, lng);

    //console.log("got here");
    // Build the search query from user input
    var src = "FacilitySearch?lat=" + lat + "&lon=" + lng;
    //console.log(src);

    $.ajax({
        url: src,
        type: 'GET',
        dataType: 'json',
        success: returnFacilities,
        error: failed
    });
}

// Return the JSON data to the page
function returnFacilities(data) {
    // Clear the previous search results
    $('#results').empty();

    // Assign the data to tmp (data.data was getting tiresome)
    var tmp = data.data;
    //console.log("Tmp variable name at index 0: " + tmp[0].attributes.name);
    

    // Table was too cluttered and hard to read, this is much more readable
    for (var i = 0; tmp.length; i++) {

        // Variables that will be used later for map info window
        var name = tmp[i].attributes.name;
        var lat = tmp[i].attributes.lat;
        var long = tmp[i].attributes.long;
        var addr = tmp[i].attributes.address.physical.address_1 + ', ' + tmp[i].attributes.address.physical.city + ' '
            + tmp[i].attributes.address.physical.state + ', ' + tmp[i].attributes.address.physical.zip;
        var phone = tmp[i].attributes.phone.main;
        var hours = 'Hours: ' + tmp[i].attributes.hours.tuesday;
        var site = tmp[i].attributes.website;

        // Send to the marker function
        //markers(name, lat, long);
        var marks = [name, lat, long, addr, phone];
        markers(marks);


        // If the website is NOT empty(null) or set to 'NULL' append Name/Address/Phone/Hours/Website else just append Name/Address/Phone/Hours
        if (site !== null && site !== 'NULL') {
            $('#results').append('<div id="facility" style="padding:2px; text-align: center;"><br>'
                + '<h5><b> ' + name + ' </b></h5>'
                + '<p>'
                + addr + '<br>'
                + hours + '<br>'
                + phone + '<br>'
                + '<a href="' + site + '">' + site + '</a>' + '<br>'
                + '</p>' + '</div>'
            );
        } else {
            $('#results').append('<div id="facility" style="padding:2px; text-align: center;"><br>'
                + '<h5><b> ' + name + ' </b></h5>'
                + '<p>'
                + addr + '<br>'
                + phone + '<br>'
                + hours + '<br>'
                + '</p>' + '</div>'
            );
        }
    }    
};

// Takes an array of info and then adds the markers to the map
function markers(arr) {

    // Builds the info for the window to display when marker is clicked
    var contentString = '<div style= "text-align:center">'
        + '<b>' + arr[0] + '</b>' + '<br>'
        + arr[3] + '<br>'
        + arr[4] + '<br>'
        + '</div>';

    // Assign the content to the window
    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    // Adds the markers based on the latitude and longitude of each facility
    var marker = new google.maps.Marker({
        position: { lat: arr[1], lng: arr[2] },
        map: map,
        title: arr[0]
    });

    // Even listener for when the marker is clicked
    marker.addListener('click', function () {
        infowindow.open(map, marker);
    });
}

// Build the Map
function buildMap(latitude, longitude) {
    // Center position of the map
    var cent = { lat: latitude, lng: longitude }

    // Build the map
    map = new google.maps.Map(document.getElementById('map'), {
        center: cent,
        zoom: 12
    });    

    // Get the div container to allow it to be displayed
    var x = document.getElementById("map");
    x.style.display = "block";
}

// Hopefully we don't hit this
function failed() {
    alert(['You fell down']);
}