﻿$(document).ready(function () {

});

var hub = $.connection.chatHub;                                         /*Reference to auto-generated proxy for the hub*/

hub.client.message = function (msg) {
    $('#discussion').append('<li>' + msg + '</li>')                     /*Returns message to chat box from ChatHub*/
}

hub.client.user = function (msg) {
    $('#discussion').append('<li>' + msg + '</li>')                     /*Returns user "You" to chat box from ChatHub*/
}

$.connection.hub.start(function () {                                    /*Start the connection to the hub*/
    $('#sendmessage').click(function () {                               /*Button click function*/
        hub.server.send($('#message').val());                           /*Send the message*/
        $('#message').val(" ");                                         /*Clear the message box*/
    })
})
 
// Function to generate an anonymous name that begins with 'anon' 
// and has an 8-digit random number following
function make_anon() {
    var anonName = "anon" + make_random_number();                            /*Variable to hold the anonymous user's name*/
    $('#welcome_spot').html('Welcome' + ' ' + anonName);                     /*Send the anonymous name to the welcome div*/
    return anonName;                                                         /*Return the anonymous name*/
}

// Function that generates 8-digit random whole numbers
function make_random_number() {
    var number = Math.round(Math.random() * 79999999) + 10000000;            /*Variable to hold the rounded random number*/
    return number;                                                           /*Return the random number*/
}

function whichName() {
    
}

var wasPressed = false;

function getBtnEventLogin() {
    alert("button pressed");
    wasPressed = true;
    return wasPressed;
}

function getBtnEventAnon() {
    wasPressed = true;
    return wasPressed;
}

function PlaySound() {
    var sound = document.getElementById('audio');
    sound.Play();
}

