﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace WhiskeyTangoFoxtrot.Hubs
{
    public class ChatHub : Hub
    {
        public override Task OnConnected()
        {
            Clients.All.NewUser(Context.User.Identity.Name);
            return base.OnConnected();
        }

        public void send(string message)
        {
            Clients.Caller.NewMessage("You: " + message);
            Clients.Others.NewMessage(Context.User.Identity.Name + ": " + message);
        }

        public async Task JoinGroup(string roomName)
        {
            await Groups.Add(Context.ConnectionId, roomName);
            await Clients.Group(roomName).GroupMessage(Context.User.Identity.Name + " joined");
        }

        public void LeaveGroup(string roomName)
        {
            Groups.Remove(Context.ConnectionId, roomName);
            Clients.Group(roomName).GroupMessage(Context.User.Identity.Name + " has left the room");
        }
    }
}