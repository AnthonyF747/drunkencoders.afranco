using WhiskeyTangoFoxtrot.DAL;
using WhiskeyTangoFoxtrot.Models;
using WhiskeyTangoFoxtrot.Repositories.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;

namespace WhiskeyTangoFoxtrot.Repositories
{
    public class ForumRepository : Repository<Forum>, IForumsRepository
    {
       public ForumRepository(WTFContext context) : base(context)
        {

        } 

        public Forum GetForum(int forumID)
        {
           return WTFContext.Fora.Where(a => a.ForumID == forumID).FirstOrDefault();
        }

        public WTFContext WTFContext
        {
            get { return Context as WTFContext; }
        }

        public IEnumerable<Forum> Forums
        {
            get { return WTFContext.Fora; }
        }
    }
}
