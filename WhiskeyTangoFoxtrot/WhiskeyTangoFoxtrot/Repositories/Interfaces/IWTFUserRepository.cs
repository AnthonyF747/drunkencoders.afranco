using WhiskeyTangoFoxtrot.Models;

namespace WhiskeyTangoFoxtrot.Repositories.Interfaces
{
    public interface IWTFUserRepository : IRepository<WTFUser>
    {
        WTFUser GetCurrentUser(int id);
    }
}
