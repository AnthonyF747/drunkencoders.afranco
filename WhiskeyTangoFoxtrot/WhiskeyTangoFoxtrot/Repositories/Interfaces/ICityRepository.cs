using WhiskeyTangoFoxtrot.Models;
using System.Collections.Generic;


namespace WhiskeyTangoFoxtrot.Repositories.Interfaces
{
    public interface ICityRepository : IRepository<City>
    {
        City GetCity(int CityID);
        IEnumerable<City> GetAllCities();
        int GetCityIDFromCityName(string city);

        IEnumerable<City> Cities { get; }
        decimal GetCityLatitude(string city);
    }
}
