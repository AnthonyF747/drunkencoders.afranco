using System.Collections.Generic;
using WhiskeyTangoFoxtrot.Models;

namespace WhiskeyTangoFoxtrot.Repositories.Interfaces
{
    public interface IForumsRepository : IRepository<Forum>
    {
        Forum GetForum(int forumID);

        IEnumerable<Forum> Forums { get; }

    }
}
