using WhiskeyTangoFoxtrot.DAL;
using WhiskeyTangoFoxtrot.Repositories.Interfaces;
using WhiskeyTangoFoxtrot.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WhiskeyTangoFoxtrot.Repositories
{
    public class CityRepository : Repository<City>, ICityRepository
    {
        public CityRepository(WTFContext context) : base(context)
        {

        }

        public City GetCity(int cityID)
        {
            return WTFContext.Cities.Where(a => a.CityID == cityID).FirstOrDefault();
        }

        public IEnumerable<City> GetAllCities()
        {
            return WTFContext.Cities.ToList();
        }

        public int GetCityIDFromCityName(string cityName)
        {
            return WTFContext.Cities.Where(a => a.City1 == cityName).Select(b => b.CityID).FirstOrDefault();
        }

        public decimal GetCityLatitude(string cityName)
        {
            return WTFContext.Cities.Where(a => a.City1 == cityName).Select(b => b.Latitude).FirstOrDefault();
        }

        public WTFContext WTFContext
        {
            get { return Context as WTFContext; }
        }

        public IEnumerable<City> Cities => throw new NotImplementedException();
    }
}
