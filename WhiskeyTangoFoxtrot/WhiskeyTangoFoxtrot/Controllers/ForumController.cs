﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using WhiskeyTangoFoxtrot.Models;
using WhiskeyTangoFoxtrot.Models.Viewmodels;
using WhiskeyTangoFoxtrot.DAL;
using WhiskeyTangoFoxtrot.Repositories.Interfaces;
using WhiskeyTangoFoxtrot.Repositories.Persistance;
using WhiskeyTangoFoxtrot.Repositories;


namespace WhiskeyTangoFoxtrot.Controllers
{
    public class ForumController : Controller
    {
        private WTFContext db = new WTFContext();

        private UnitOfWork unitOfWork = new UnitOfWork(new WTFContext());

        private IForumPostRepository _forumPost;
        private IForumsRepository _forum;
        private IWTFUserRepository _user;


        public ForumController() { }

        public ForumController(IForumsRepository forums, IForumPostRepository forumPostRepository, IWTFUserRepository wTFUser)
        {
            _forum = forums;
            _forumPost = forumPostRepository;
            _user = wTFUser;
        }

        public ForumController(IForumPostRepository MOCKforumPostRepository)
        {
            _forumPost = MOCKforumPostRepository;

        }

        // GET: Forum
        public ActionResult Index()
        {
            var viewmodel = CreateViewModel();
            return View(viewmodel);
        }

        // GET: Forum/Details/5
        [Authorize]
        public ActionResult Details(int id)
        {
            var viewmodel = CreateViewModel(id);
            Forum forum = unitOfWork.Forums.Get(id);
            if (forum == null)
            {
                return HttpNotFound();
            }
            /*
            if (forum.IsActive != null && GetUserID() != unitOfWork.Forums.Forums.Select(a => a.WTFUserID).FirstOrDefault())
            {
                return Redirect("~/WTFErrors/Error.html");
            }
            */
            viewmodel.Forums.Select(a => a.ForumID == forum.ForumID).FirstOrDefault();
            return View(viewmodel);
        }

        // GET: Forum/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.WTFUserID = new SelectList(db.WTFUsers, "WTFUserID", "Username");
            return View();
        }

        // POST: Forum/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ForumName,Message,IsActive,IsMemberOnly")] Forum forum)
        {
            forum.ForumID = db.Fora.Count() + 1;
            forum.WTFUserID = GetUserID();
            if (ModelState.IsValid)
            {
                db.Fora.Add(forum);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.WTFUserID = new SelectList(db.WTFUsers, "WTFUserID", "Username", forum.WTFUserID);
            return View(forum);
        }

        // GET: Forum/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Forum forum = unitOfWork.Forums.Get((int)id);
            if (forum == null)
            {
                return HttpNotFound();
            }

            var user = GetUserID();
            if (forum.WTFUserID != user)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.WTFUserID = new SelectList(unitOfWork.WTFUser.GetAll(), "WTFUserID", "Username", forum.WTFUserID);
            return View(forum);
        }

        // POST: Forum/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ForumID,WTFUserID,ForumName,Message,IsActive,IsMemberOnly")] Forum forum)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Forums.SetModified(forum);
                unitOfWork.Complete();

                /*
                db.Entry(forum).State = EntityState.Modified;
                db.SaveChanges();
                */
                return RedirectToAction("Index");
            }
            ViewBag.WTFUserID = new SelectList(unitOfWork.WTFUser.GetAll(), "WTFUserID", "Username", forum.WTFUserID);
            return View(forum);
        }

        // GET: Forum/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Forum forum = unitOfWork.Forums.Get((int)id);
            if (forum == null)
            {
                return HttpNotFound();
            }
            var userID = GetUserID();

            if (userID != forum.WTFUserID)
            {
                return new HttpUnauthorizedResult();
            }
            return View(forum);
        }

        // POST: Forum/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Forum forum = unitOfWork.Forums.Get(id);
            unitOfWork.Forums.Remove(forum);
            unitOfWork.Complete();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        [Authorize]
        private ForumViewModel CreateViewModel(int? forumID)
        {

            //int UserID = GetUserID();

            ForumViewModel forumViewModel = new ForumViewModel
            {
                WTFUsers = unitOfWork.WTFUser.GetAll(),

                ForumPost = unitOfWork.Posts.GetAll().Where(a => a.ForumID == forumID),

                Forums = unitOfWork.Forums.Find(a => a.ForumID == forumID)
            };

            return forumViewModel;
        }

        [Authorize]
        private ForumViewModel CreateViewModel()
        {

            //int UserID = GetUserID();

            ForumViewModel forumViewModel = new ForumViewModel
            {
                WTFUsers = unitOfWork.WTFUser.GetAll(),

                ForumPost = unitOfWork.Posts.GetAll(),

                Forums = unitOfWork.Forums.GetAll()
            };

            return forumViewModel;
        }

        [Authorize]
        private int GetUserID()
        {
            string user1 = User.Identity.GetUserId();

            WTFUser user = db.WTFUsers.Where(a => a.AspNetIdentityID == user1).FirstOrDefault();
            
            int UserID = user.WTFUserID;

            return UserID;
        }

        [Authorize]
        [HttpPost]
        public ActionResult replyTo(int postingID, string replyString)//int postingID, string replyString)
        {
            //var forumID = db.ForumPosts.Where(b => b.ForumPostID == postingID).Select(a => a.ForumID).FirstOrDefault();
            var forumID = unitOfWork.Posts.Get(postingID).ForumID;

            //ForumPost forumPost = db.ForumPosts.Create();
            ForumPost forumPost = new ForumPost();
            forumPost.ForumPostID = getNextIndex();
            forumPost.ForumID = forumID;
            forumPost.WTFUserID = GetUserID();
            forumPost.ReplyID = postingID;
            forumPost.PostMessage = replyString;

            if (ModelState.IsValid)
            {
                //   db.ForumPosts.Add(forumPost);
                //  db.SaveChanges();
                unitOfWork.Posts.Get(postingID).Replies += 1;
                unitOfWork.Posts.Add(forumPost);
                unitOfWork.Complete();

                replyAlert();
                return View(Details(forumID));

            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public int getNextIndex()
        {
            //int index = db.ForumPosts.Count() + 1;
            int index = unitOfWork.Posts.GetAll().Count() + 1;
            return index;
        }

        public int getNextIndex(int[] arr)
        {
            int index = arr.Count() + 1;

            return index;
        }
        [Authorize]
        [HttpPost]
        public ActionResult replyToMain(int forumid, string replyString)
        {
            var forumID = db.Fora.Where(b => b.ForumID == forumid).Select(a => a.ForumID).FirstOrDefault();

            ForumPost forumPost = db.ForumPosts.Create();
            forumPost.ForumPostID = db.ForumPosts.Count() + 1;
            forumPost.ForumID = forumID;
            forumPost.WTFUserID = GetUserID();
            forumPost.ReplyID = null;
            forumPost.PostMessage = replyString;

            if (ModelState.IsValid)
            {
                db.ForumPosts.Add(forumPost);
                db.SaveChanges();
                return View(Details(forumID));

            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        [Authorize]
        public void replyAlert()
        {

        }
    }
}
